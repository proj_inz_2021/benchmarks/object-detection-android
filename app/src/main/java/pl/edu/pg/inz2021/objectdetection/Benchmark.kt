package pl.edu.pg.inz2021.objectdetection

import android.content.res.AssetFileDescriptor
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.graphics.RectF
import android.os.Build
import android.util.Log
import android.widget.ProgressBar
import android.widget.TextView
import org.json.JSONArray
import org.json.JSONObject
import org.tensorflow.lite.DataType
import org.tensorflow.lite.Interpreter
import org.tensorflow.lite.Tensor
import org.tensorflow.lite.gpu.CompatibilityList
import org.tensorflow.lite.gpu.GpuDelegate
import org.tensorflow.lite.nnapi.NnApiDelegate;
import org.tensorflow.lite.support.image.TensorImage
import org.tensorflow.lite.support.tensorbuffer.TensorBuffer
import org.tensorflow.lite.task.vision.detector.ObjectDetector
import java.io.File
import java.io.FileInputStream
import java.lang.Exception
import java.lang.RuntimeException
import java.nio.ByteBuffer
import java.nio.ByteOrder
import java.nio.MappedByteBuffer
import java.nio.channels.FileChannel
import java.util.*
import kotlin.math.round
import kotlin.math.roundToInt

enum class Device{
    CPU,
    GPU,
    NNAPI
}

class Benchmark(
    private val context: MainActivity,
    private val benchmarkProgressBar: ProgressBar,
    private val stageProgressBar: ProgressBar,
    private val progressMessage: TextView
) {
    private val TEST_NUMBER = 1
    private val IMAGE_NUMBER = 20
    private val NUM_THREADS = Runtime.getRuntime().availableProcessors()
    private var detector: ObjectDetector? = null
    private var interpreterGPU: Interpreter? = null
    private var interpreterCPU: Interpreter? = null
    private var interpreterNnAPI: Interpreter? = null
    private var gpuDelegate: GpuDelegate? = null
    private var nnApiDelegate: NnApiDelegate? = null
//    private val compatList = CompatibilityList()
    private var currentModelName: String = ""

    fun run() {
        val deviceName = getDeviceName()?.replace(' ', '-')
        val modelNames: Array<String> = context.assets.list("models")!!
        val imageNames = context.assets.list("dataset")!!
        var lastDelay: Long = 0
        val variants = Device.values()
//        val variants = arrayOf(
//            if(compatList.isDelegateSupportedOnThisDevice){
//                // if the device has a supported GPU, add the GPU variant
//                Device.values()
//            } else {
//                // if the GPU is not supported, run only on CPU
//                Device.CPU
//            })

        for (test_id in 0 until TEST_NUMBER) {
            for ((model_index, modelName) in modelNames.withIndex()) {
                currentModelName = modelName
//                when(model_index){ !in arrayOf(10) -> continue}
//                initDetector("models/$modelName")
                initCPUInterpreter("models/$modelName")
                initGPUInterpreter("models/$modelName")
                initNnApiInterpreter("models/$modelName")

                for ((variant_id, variant) in variants.withIndex()) {
                    updateProgress(
                        benchmarkProgressBar,
                        test_id * modelNames.size * variants.size + + model_index * variants.size + variant_id,
                        TEST_NUMBER * modelNames.size * variants.size
                    )
                    val delays = JSONArray()
                    updateProgress(stageProgressBar, 0, imageNames.size)
                    for ((image_index, imageName) in imageNames.withIndex()) {
                        context.runOnUiThread(Runnable {
                            progressMessage.text =
                                "Test: ${test_id + 1}/${TEST_NUMBER} \n\n" +
                                        "Model: $modelName (${model_index + 1}/${modelNames.size})\n\n" +
                                        "Device: $variant (${variant_id + 1}/${variants.size})\n\n" +
                                        "Image nr: ${image_index + 1}/${imageNames.size}\n\n" +
                                        "Last inference Time: $lastDelay"
                        })
                        val benchmarkResult = when(variant){
                            Device.CPU -> runObjectDetection(interpreterCPU!!,getBitmapFromImageFile("dataset/$imageName")
                            )
                            Device.GPU -> runObjectDetection(interpreterGPU!!,getBitmapFromImageFile("dataset/$imageName"))
                            Device.NNAPI -> runObjectDetection(interpreterNnAPI!!,getBitmapFromImageFile("dataset/$imageName"))
                            else -> throw Exception("Variant: $variant isn't supported")
                        }
                        lastDelay = benchmarkResult.delay
                        Log.i("Benchmark","Delay $variant: ${benchmarkResult.delay}")

                        delays.put(
                            JSONObject()
                                .put("image", imageName)
                                .put("time", benchmarkResult.delay))
                        saveResultsToFile(
                            "results/${deviceName}_$variant/$modelName/test_$test_id/boxes/${imageName.substring(0, imageName.length - 4)}.txt",
                            benchmarkResult)
                        updateProgress(stageProgressBar, image_index+1, imageNames.size)
                    }
                    saveJSONToFile("results/${deviceName}_$variant/$modelName/test_$test_id/delays.json", delays)
                }
            }
        }
        updateProgress(benchmarkProgressBar, 1, 1)
        updateProgress(stageProgressBar, 1, 1)
    }

    private fun updateProgress(progress: ProgressBar, value: Int, max: Int) {
        context.runOnUiThread(Runnable {
            progress.max = 0
            progress.progress = 0
            progress.max = max
            progress.progress = value
            progress.refreshDrawableState()
        })
    }
    private fun initCPUInterpreter(modelName: String) {
        interpreterCPU?.close()
        val options = Interpreter.Options()
        options.setNumThreads(NUM_THREADS);
        options.setUseXNNPACK(true);
        interpreterCPU = Interpreter(loadModelFile(modelName), options)
    }
    private fun initGPUInterpreter(modelName: String) {
        interpreterGPU?.close()
        gpuDelegate?.close()
        val delegateOptions = CompatibilityList().bestOptionsForThisDevice
        gpuDelegate = GpuDelegate(delegateOptions)
        val options = Interpreter.Options().addDelegate(gpuDelegate)
        options.setNumThreads(NUM_THREADS);
        options.setUseXNNPACK(true);
        interpreterGPU = Interpreter(loadModelFile(modelName), options)
    }

    private fun initNnApiInterpreter(modelName: String) {
        interpreterNnAPI?.close()
        nnApiDelegate?.close()
        nnApiDelegate = NnApiDelegate()
        val options = Interpreter.Options().addDelegate(nnApiDelegate)
        interpreterNnAPI = Interpreter(loadModelFile(modelName), options)
    }

    private fun loadModelFile(modelPath: String): MappedByteBuffer {
        // use to get description of file
        val fileDescriptor: AssetFileDescriptor = context.assets.openFd(modelPath)
        val inputStream: FileInputStream = FileInputStream(fileDescriptor.fileDescriptor)
        val fileChannel: FileChannel = inputStream.channel
        val startOffset: Long = fileDescriptor.startOffset
        val declaredLength: Long = fileDescriptor.declaredLength
        return fileChannel.map(FileChannel.MapMode.READ_ONLY, startOffset, declaredLength)
    }

    private fun initDetector(modelName: String) {
        detector?.close()
        val options = ObjectDetector.ObjectDetectorOptions.builder()
            .setMaxResults(-1)
            .build()
        detector = ObjectDetector.createFromFileAndOptions(
            this.context,
            modelName,
            options
        )
    }
    private fun runObjectDetection(interpreter: Interpreter, bitmap: Bitmap): DetectionResult {
        val input = initInputArray(interpreter, bitmap)
        val (outputMap, outputIndexes) = initOutputMap(interpreter)

        val startTime = System.nanoTime()
        interpreter.runForMultipleInputsOutputs(input, outputMap)
        val endTime = System.nanoTime()
        val inferenceTime = (endTime - startTime) / 1000000 //divide by 1 000 000 to get milliseconds.
//        Log.d("Detection", "GPU time: $inferenceTime")
        val objects = convertOutput(interpreter, outputMap, outputIndexes, bitmap)
        return DetectionResult(inferenceTime, objects)
    }

    private fun runObjectDetectionCPU(bitmap: Bitmap): DetectionResult {
        // Step 1: Create TFLite's TensorImage object
        val image = TensorImage.fromBitmap(bitmap)
        // Step 3: Feed given image to the detector
        val testedDetector = detector

        var startTime: Long = System.nanoTime()
        val results = if (testedDetector != null) testedDetector.detect(image) else throw NullPointerException()
        var endTime: Long = System.nanoTime()

        var inferenceTime: Long = (endTime - startTime) / 1000000 //divide by 1 000 000 to get milliseconds.
        Log.i("Detection", "CPU time: $inferenceTime")

        val objects = LinkedList(results.map {
            // Get the top-1 category and craft the result
            val category = it.categories.first()
            DetectedObject(category.label, category.score, it.boundingBox)
        })
        return DetectionResult(inferenceTime, objects)
    }

    private fun convertOutput(interpreter: Interpreter, output: HashMap<Int, Any>, outputIndexes: List<Int>, originalBitmap: Bitmap): LinkedList<DetectedObject>{
        val objects = LinkedList<DetectedObject>()
        val (tensorArrays, sortedIndexes) = (output.values zip outputIndexes).sortedWith(compareBy { it.second } ).unzip()//.map{ it.first }
        val dataType = interpreter.getOutputTensor(0).dataType()

        var boxes: Array<FloatArray> = Array(0){ floatArrayOf()}
        var classes: FloatArray = floatArrayOf()
        var scores: FloatArray = floatArrayOf()
        var noD: Int = 0
        when(dataType){
            DataType.UINT8 -> {
                var byte_boxes: Array<ByteArray> = Array(0){ byteArrayOf()}
                var byte_classes: ByteArray = byteArrayOf()
                var byte_scores: ByteArray = byteArrayOf()
                var byte_noD: Byte = 0
                val tensorIndex = Array<Int>(4){0}

                for ((i, tensor_array)  in (tensorArrays as ArrayList<*>).withIndex()){
                    if (tensor_array is ByteArray){
                        byte_noD = tensor_array[0]
                        tensorIndex[3] = i
                    }
                    else if( (tensor_array as Array<*>)[0] is Array<*> ){
                        byte_boxes = (tensor_array[0] as Array<ByteArray>)
                        tensorIndex[0] = i
                    }else{
                        val scoresQuantParams = interpreter.getOutputTensor(i).quantizationParams()
                        val testVal = ((tensor_array[0] as ByteArray)[0].toUByte() - scoresQuantParams.zeroPoint.toUByte()).toInt() * scoresQuantParams.scale

                        if (testVal < 1 && testVal > 0) {
                            byte_scores = (tensor_array[0] as ByteArray)
                            tensorIndex[2] = i
                        }
                        else {
                            byte_classes = (tensor_array[0] as ByteArray)
                            tensorIndex[1] = i
                        }
                    }
                }

                val boxesQuantParams = interpreter.getOutputTensor(tensorIndex[0]).quantizationParams()
                val classesQuantParams = interpreter.getOutputTensor(tensorIndex[1]).quantizationParams()
                val scoresQuantParams = interpreter.getOutputTensor(tensorIndex[2]).quantizationParams()
                val noDQuantParams = interpreter.getOutputTensor(tensorIndex[3]).quantizationParams()
                boxes = byte_boxes.map{ box -> (box.map{ (it.toUByte() - boxesQuantParams.zeroPoint.toUByte()).toInt() * boxesQuantParams.scale}.toFloatArray())}.toTypedArray()
                classes = byte_classes.map{ ((it.toUByte() - classesQuantParams.zeroPoint.toUByte()).toInt() * classesQuantParams.scale).roundToInt().toFloat()}.toFloatArray()
                scores = byte_scores.map{ (it.toUByte() - scoresQuantParams.zeroPoint.toUByte()).toInt() * scoresQuantParams.scale}.toFloatArray()
                noD = ((byte_noD.toUByte() - noDQuantParams.zeroPoint.toUByte()).toInt() * noDQuantParams.scale).roundToInt()

//                if (this.currentModelName.contains("mobilenet")){
//                    val boxesQuantParams = interpreter.getOutputTensor(outputIndexes.indexOf(sortedIndexes[1])).quantizationParams()
//                    val classesQuantParams = interpreter.getOutputTensor(outputIndexes.indexOf(sortedIndexes[3])).quantizationParams()
//                    val scoresQuantParams = interpreter.getOutputTensor(outputIndexes.indexOf(sortedIndexes[0])).quantizationParams()
//                    val noDQuantParams = interpreter.getOutputTensor(outputIndexes.indexOf(sortedIndexes[2])).quantizationParams()
//                    boxes = (tensorArrays[1] as Array<Array<ByteArray>>)[0].map{ box -> (box.map{ (it.toUByte() - boxesQuantParams.zeroPoint.toUByte()).toInt() * boxesQuantParams.scale}.toFloatArray())}.toTypedArray()
//                    classes = (tensorArrays[3] as Array<ByteArray>)[0].map{ ((it.toUByte() - classesQuantParams.zeroPoint.toUByte()).toInt() * classesQuantParams.scale).roundToInt().toFloat()}.toFloatArray()
//                    scores = (tensorArrays[0] as Array<ByteArray>)[0].map{ (it.toUByte() - scoresQuantParams.zeroPoint.toUByte()).toInt() * scoresQuantParams.scale}.toFloatArray()
//                    noD = (((tensorArrays[2] as ByteArray)[0].toUByte() - noDQuantParams.zeroPoint.toUByte()).toInt() * noDQuantParams.scale).roundToInt()
//                }else {
//                    val boxesQuantParams = interpreter.getOutputTensor(outputIndexes.indexOf(sortedIndexes[0])).quantizationParams()
//                    val classesQuantParams = interpreter.getOutputTensor(outputIndexes.indexOf(sortedIndexes[1])).quantizationParams()
//                    val scoresQuantParams = interpreter.getOutputTensor(outputIndexes.indexOf(sortedIndexes[2])).quantizationParams()
//                    val noDQuantParams = interpreter.getOutputTensor(outputIndexes.indexOf(sortedIndexes[3])).quantizationParams()
//                    boxes = (tensorArrays[0] as Array<Array<ByteArray>>)[0].map{ box -> (box.map{ (it.toUByte() - boxesQuantParams.zeroPoint.toUByte()).toInt() * boxesQuantParams.scale}.toFloatArray())}.toTypedArray()
//                    classes = (tensorArrays[1] as Array<ByteArray>)[0].map{ ((it.toUByte() - classesQuantParams.zeroPoint.toUByte()).toInt() * classesQuantParams.scale).roundToInt().toFloat()}.toFloatArray()
//                    scores = (tensorArrays[2] as Array<ByteArray>)[0].map{ (it.toUByte() - scoresQuantParams.zeroPoint.toUByte()).toInt() * scoresQuantParams.scale}.toFloatArray()
//                    noD = (((tensorArrays[3] as ByteArray)[0].toUByte() - noDQuantParams.zeroPoint.toUByte()).toInt() * noDQuantParams.scale).roundToInt()
//                }
            }
            else -> {
                try {
                    for (tensor_array  in tensorArrays as ArrayList<*>){
                        if (tensor_array is FloatArray){
                            noD = tensor_array[0].toInt()
                        }
                        else if( (tensor_array as Array<*>)[0] is Array<*> ){
                                boxes = (tensor_array[0] as Array<FloatArray>)
                        }else{
                            if (((tensor_array[0] as FloatArray)[0]) < 1 && ((tensor_array[0] as FloatArray)[0]) > 0) {
                                scores = (tensor_array[0] as FloatArray)
                            }
                            else {
                                classes = (tensor_array[0] as FloatArray)
                            }
                        }
                    }
                }catch (e: Exception){
                    throw Exception(
                        "Model name: ${this.currentModelName}\n" +
                        "Indexes: ${sortedIndexes[0]}, ${sortedIndexes[1]}, ${sortedIndexes[2]}, ${sortedIndexes[3]}",e)
                }
            }
        }
//        if (classes.isEmpty()){
//            Log.d("a","test")
//        }
        for(d in 0 until noD){
            val label = CocoLabels.getName(classes[d].toInt()+1)
            val score = scores[d]
            val boundingBox = RectF(
                round(boxes[d][1] * originalBitmap.width),
                round(boxes[d][0] * originalBitmap.height),
                round(boxes[d][3] * originalBitmap.width),
                round(boxes[d][2] * originalBitmap.height)
            )
            objects.push(DetectedObject(label,score,boundingBox))
        }
        objects.reverse()
        return objects
    }

    private fun initOutputMap(interpreter: Interpreter): Pair<HashMap<Int, Any>, List<Int>> {
        val outputMap = HashMap<Int, Any>()
        val dataType = interpreter.getOutputTensor(0).dataType()
        val tensors = arrayOf(
            interpreter.getOutputTensor(0),
            interpreter.getOutputTensor(1),
            interpreter.getOutputTensor(2),
            interpreter.getOutputTensor(3),
        )

        for ((i, tensor) in tensors.withIndex()) {
            val shape = tensor.shape()
            when (shape.size) {
                1 -> outputMap[i] = if (dataType == DataType.FLOAT32) FloatArray(shape[0]) else ByteArray(shape[0])
                2 -> outputMap[i] = if (dataType == DataType.FLOAT32) {
                    Array(shape[0]) { FloatArray(shape[1]) }
                }else {
                    Array(shape[0]) { ByteArray(shape[1]) }
                }
                3 -> outputMap[i] =if (dataType == DataType.FLOAT32) {
                    Array(shape[0]) {
                        Array(shape[1]) {
                            FloatArray(shape[2])
                        }
                    }
                }else {
                    Array(shape[0]) {
                        Array(shape[1]) {
                            ByteArray(shape[2])
                        }
                    }
                }
            }
        }
        return Pair(outputMap, tensors.map{ it.index() })
    }

    private fun initInputArray(interpreter: Interpreter, bitmap: Bitmap): Array<ByteBuffer> {
        val inputShape = interpreter.getInputTensor(0).shape()
        val dataType = interpreter.getInputTensor(0).dataType()
        val scaledBitmap = Bitmap.createScaledBitmap(
            bitmap,
            inputShape[1],
            inputShape[2],
            false
        )

        return when (dataType) {
            DataType.UINT8 -> {
                arrayOf(TensorImage.fromBitmap(scaledBitmap).buffer)
            }
            DataType.FLOAT32 -> {
                val bytesPerChannel = 4
                val inputChannels = 3
                val inputBuffer = ByteBuffer.allocateDirect(
                    bytesPerChannel * scaledBitmap.height * scaledBitmap.width * inputChannels
                )
                inputBuffer.order(ByteOrder.nativeOrder())
                inputBuffer.rewind()

                val mean = 127.5f
                val std = 127.5f
                val intValues = IntArray(scaledBitmap.width * scaledBitmap.height)
                scaledBitmap.getPixels(
                    intValues,
                    0,
                    scaledBitmap.width,
                    0,
                    0,
                    scaledBitmap.width,
                    scaledBitmap.height
                )
                for (pixelValue in intValues) {
                    inputBuffer.putFloat(((pixelValue shr 16 and 0xFF) - mean) / std)
                    inputBuffer.putFloat(((pixelValue shr 8 and 0xFF) - mean) / std)
                    inputBuffer.putFloat(((pixelValue and 0xFF) - mean) / std)
                }
                arrayOf(inputBuffer)
            }
            else -> arrayOf()
        }
    }

    private fun getBitmapFromImageFile(imageFilename: String): Bitmap {
        return BitmapFactory.decodeStream(context.assets.open(imageFilename))
    }

    private fun saveJSONToFile(fileName: String, data: JSONArray) {
        val folder = this.context.getExternalFilesDir(null).toString()
        val outputFile = File("$folder/$fileName")
        outputFile.parentFile.mkdirs()
        outputFile.writeText(data.toString(4))
    }

    private fun saveResultsToFile(fileName: String, results: DetectionResult) {
        val folder = this.context.getExternalFilesDir(null).toString()
        val outputFile = File("$folder/$fileName")
        outputFile.parentFile.mkdirs()
        val outputBuilder = StringBuilder()
        for (obj in results.objects) {
            outputBuilder.append("${obj.label.replace(' ', '_')} ")
            outputBuilder.append("${obj.score} ")
            outputBuilder.append("${obj.boundingBox.left.toInt()} ${obj.boundingBox.top.toInt()} ")
            outputBuilder.append("${obj.boundingBox.right.toInt()} ${obj.boundingBox.bottom.toInt()}")
            outputBuilder.append("\n")
        }
        outputFile.writeText(outputBuilder.toString())
    }

    fun getDeviceName(): String? {
        val manufacturer = Build.MANUFACTURER
        val model = Build.MODEL
        return if (model.lowercase(Locale.getDefault()).startsWith(manufacturer.lowercase(Locale.getDefault()))) {
            capitalize(model)
        } else {
            capitalize(manufacturer) + " " + model
        }
    }


    private fun capitalize(s: String?): String {
        if (s == null || s.length == 0) {
            return ""
        }
        val first = s[0]
        return if (Character.isUpperCase(first)) {
            s
        } else {
            Character.toUpperCase(first).toString() + s.substring(1)
        }
    }
}

data class DetectedObject(val label: String, val score: Float, val boundingBox: RectF)
data class DetectionResult(val delay: Long, val objects: List<DetectedObject>)
